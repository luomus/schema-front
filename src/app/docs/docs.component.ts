import { Component, OnInit, OnDestroy } from '@angular/core';
import { MetadataService } from '../shared/metadata.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Metadata } from '../shared/model/Metadata';
import { Observable, Subscription } from 'rxjs';
import { Property } from '../shared/model/Property';
import { WindowRef } from '../shared/WindowRef';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.css']
})
export class DocsComponent implements OnInit, OnDestroy {

  data: Meta = {};
  highLight = '';
  altId = '';
  activeLang;
  static classes: Metadata[];
  private subLang: Subscription;
  private subParams: Subscription;

  constructor(
    private winRef: WindowRef,
    private metadataService:MetadataService,
    private route: ActivatedRoute,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this.subLang = Observable.combineLatest(
      Observable.merge(
        this.translateService.onLangChange.do(() => {
          if (this.activeLang !== this.translateService.currentLang) {
            DocsComponent.classes = undefined;
            this.activeLang = this.translateService.currentLang;
          }}),
        Observable.of(this.translateService.currentLang)
      ),
      this.route.params,
      (lang, params) => params
    ).switchMap((params: Params) => {
        const classes$ = this.metadataService
          .allClasses(this.translateService.currentLang)
          .map(classes => classes.map(meta => {
              meta['ns'] = meta['class'].replace(meta['shortName'], '').slice(0, -1);
              return meta;
            }).sort((a, b) => a.shortName.localeCompare(b.shortName)))
          .do(classes => DocsComponent.classes = classes);
        if (!params['classId'] && !params['propertyId'] && !params['altId']) {
          return (DocsComponent.classes ? Observable.of(DocsComponent.classes) : classes$)
              .map(classes => (<Meta>{classes: classes}));
        } else if (params['classId']) {
          this.highLight = params['propertyId'] || '';
          const property$:Observable<Meta> = this.metadataService
            .findClassById(params['classId'], this.translateService.currentLang)
            .combineLatest(
              this.metadataService
                .findPropertiesByClassId(params['classId'], this.translateService.currentLang)
                .map(properties => properties
                  .map(property => {
                    property['ns'] = property['property'].replace(property['shortName'], '').slice(0, -1);
                    return property;
                  })
                  .sort((a, b) => a.shortName.localeCompare(b.shortName))),
              (meta: Metadata, properties: any) => {
                meta['properties'] = properties;
                return meta;
              })
            .map(classData => (<Meta>{classData: classData}));
          return DocsComponent.classes ? property$ : classes$.switchMap(() => property$);
        } else if (params['propertyId']) {
          return classes$.switchMap(() => this.metadataService.fincPropertyById(params['propertyId'], this.translateService.currentLang))
            .map(property => (<Meta>{property: property}))
        } else if (params['altId']) {
          this.altId = params['altId'];
          return this.metadataService.findRangeById(params['altId'], this.translateService.currentLang)
            .map(alt => (<Meta>{alt: alt}))
        } else {
          return Observable.of({});
        }
      }
    ).subscribe((data: Meta) => this.data = data);
    this.winRef.nativeWindow.scrollTo(0, 0);
  }

  ngOnDestroy() {
    if (this.subLang) {
      this.subLang.unsubscribe();
    }
    if (this.subParams) {
      this.subParams.unsubscribe();
    }
  }

  getRangeType(id) {
    if (id.indexOf('xsd') === 0 || id.indexOf('rdf') === 0 || !DocsComponent.classes) {
      return 'xsd';
    }
    for(var i = 0, len = DocsComponent.classes.length; i < len; i++) {
      if(DocsComponent.classes[i]['class'] === id) {
        return 'class'
      }
    }
    return 'alt';
  }
}

interface Meta {
  classes?: Metadata[],
  classData?: any,
  property?: Property,
  alt?: any;
}
