import { Component } from '@angular/core';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { MetadataService } from './shared/metadata.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private searchStr: string;
  private dataService: AutocompleteData;

  constructor(
    private translate: TranslateService,
    private metadataService:MetadataService,
    private router: Router
  ) {
    this.translate.use('en');
    this.dataService = new AutocompleteData(this.metadataService, this.translate);
  }

  setLang(lang:string) {
    this.translate.use(lang);
  }

  onSelect(item) {
    if (!item) {
      return;
    }
    const pathMap = {
      c: 'class',
      p: 'property',
      a: 'alt'
    };
    this.router.navigate(['/' + pathMap[item.type] + '/' + item.title]);
  }
}

export class AutocompleteData extends Subject<CompleterItem[]> implements CompleterData {
  private data;
  constructor(
    private metadataService:MetadataService,
    private translate: TranslateService
  ) {
    super();
    translate.onLangChange.subscribe(() => this.data = null);
  }

  public search(term: string): void {
    let lowerTerm = term.toLowerCase();
    const cached$ = Observable.of(this.data);
    const meta$ = Observable.combineLatest(
      this.metadataService.allClasses(this.translate.currentLang)
        .map(classes => classes.map(meta => ({
          type: 'c',
          title: meta['class'],
          description: '(c) ' + (meta['label'] || ''),
          search: (meta['class'] || '').toLowerCase() + ' ' + (meta['label'] || '').toLowerCase()
        }))),
      this.metadataService.allProperties(this.translate.currentLang)
        .map(properties => properties.map(property => ({
          type: 'p',
          title: property['property'],
          description: '(p) ' + (property['label'] || ''),
          search: (property['property'] || '').toLowerCase() + ' ' + (property['label'] || '').toLowerCase()
        }))),
      this.metadataService.allRanges(this.translate.currentLang)
        .map(alts => Object.keys(alts).map(alt => ({
          type: 'a',
          title: alt,
          values: alts[alt].map(item => item['id'].toLowerCase()),
          description: '(a)',
          search: (alt || '').toLowerCase()
        }))),
      (c, p, a) => [...c,...p,...a]
    );
    let acceptedTypes = ['c', 'p', 'a'];
    switch (lowerTerm.substr(0,2)) {
      case 'c:':
      case 'p:':
      case 'a:':
        acceptedTypes = [lowerTerm.substr(0,1)];
        lowerTerm = lowerTerm.substr(2);
        break;
      default:
    }
    lowerTerm = lowerTerm.trim();
    if (lowerTerm.length < 1) {
      this.next([]);
      return;
    }
    (this.data ? cached$ : meta$)
      .map(data => this.next(data.filter(data => {
        if (acceptedTypes.indexOf(data['type']) > -1) {
          if (data['search'].includes(lowerTerm)) {
            return true;
          }
          if (data['values'] && data['values'].includes(lowerTerm)) {
            return true;
          }
        }
        return false;
      }))
      ).subscribe();
  }

  public cancel() {
  }
}
