import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DocsComponent } from './docs/docs.component';
import { MetadataService } from './shared/metadata.service';
import { WindowRef } from './shared/WindowRef';
import { HttpClient, HttpClientModule } from '@angular/common/http';

export function translateFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DocsComponent
  ],
  imports: [
    BrowserModule,
    Ng2CompleterModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'class',
        component: DocsComponent,
      },
      {
        path: 'class/:classId',
        component: DocsComponent,
      },
      {
        path: 'class/:classId/:propertyId',
        component: DocsComponent,
      },
      {
        path: 'property/:propertyId',
        component: DocsComponent,
      },
      {
        path: 'alt/:altId',
        component: DocsComponent
      }
    ]),
  ],
  providers: [
    WindowRef,
    MetadataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
