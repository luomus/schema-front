import { Http, Headers, RequestOptionsArgs, Response, URLSearchParams } from '@angular/http';
import { Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Property } from './model/Property';
import { Metadata } from './model/Metadata';
import { PagedResult } from './model/PagedResult';

@Injectable()
export class MetadataService {
  public defaultHeaders: Headers = new Headers();

  protected basePath = 'https://laji.fi/api';

  private cache = {};

  constructor(protected http: Http) {
  }

  /**
   * Returns info about all the classes
   *
   * @param lang
   */
  public allClasses(lang?: string): Observable<Metadata[]> {
    if (this.cache['classes' + lang]) {
      return Observable.of(this.cache['classes' + lang]);
    }
    const path = this.basePath + '/metadata/classes';

    let queryParameters = new URLSearchParams();
    let headerParams = this.defaultHeaders;
    if (lang !== undefined) {
      queryParameters.set('lang', lang);
    }

    let requestOptions: RequestOptionsArgs = {
      method: 'GET',
      headers: headerParams,
      search: queryParameters
    };

    return this.http.request(path, requestOptions)
      .map((response: Response) => {
        if (response.status === 204) {
          return undefined;
        } else {
          return response.json();
        }
      })
      .map(result => result.results)
      .do(classes => this.cache['classes' + lang] = classes);
  }

  /**
   * Returns info about all the properties
   *
   * @param lang
   */
  public allProperties(lang?: string): Observable<Property[]> {
    if (this.cache['properties' + lang]) {
      return Observable.of(this.cache['properties' + lang]);
    }
    const path = this.basePath + '/metadata/properties';

    let queryParameters = new URLSearchParams();
    let headerParams = this.defaultHeaders;
    if (lang !== undefined) {
      queryParameters.set('lang', lang);
    }

    let requestOptions: RequestOptionsArgs = {
      method: 'GET',
      headers: headerParams,
      search: queryParameters
    };

    return this.http.request(path, requestOptions)
      .map((response: Response) => {
        if (response.status === 204) {
          return undefined;
        } else {
          return response.json();
        }
      })
      .map(result => result.results)
      .do(properties => this.cache['properties' + lang] = properties);
  }

  /**
   * Returns all ranges that are of select (alt) type
   *
   * @param lang
   */
  public allRanges(lang?: string): Observable<any> {
    if (this.cache['ranges' + lang]) {
      return Observable.of(this.cache['ranges' + lang]);
    }
    const path = this.basePath + '/metadata/ranges';

    let queryParameters = new URLSearchParams();
    let headerParams = this.defaultHeaders;
    if (lang !== undefined) {
      queryParameters.set('lang', lang);
    }

    let requestOptions: RequestOptionsArgs = {
      method: 'GET',
      headers: headerParams,
      search: queryParameters
    };

    return this.http.request(path, requestOptions)
      .map((response: Response) => {
        if (response.status === 204) {
          return undefined;
        } else {
          return response.json();
        }
      })
      .do(ranges => this.cache['ranges' + lang] = ranges);
  }

  /**
   * Returns info about the class
   *
   * @param _class
   * @param lang
   */
  public findClassById(_class: string, lang?: string): Observable<Property> {
    return this.allClasses(lang)
      .map(classes => {
        for (let i = 0, len = classes.length; i < len; i++) {
          if (classes[i]['class'] === _class) {
            return classes[i];
          }
        }
        return {'class': _class};
      });
  }

  /**
   * Returns info about the property
   *
   * @param property
   * @param lang
   * @returns {Observable<Property>}
   */
  public fincPropertyById(property: string, lang?: string): Observable<Property> {
    return this.allProperties(lang)
      .map(properties => {
        for (let i = 0, len = properties.length; i < len; i++) {
          if (properties[i]['property'] === property) {
            return properties[i];
          }
        }
        return {'property': property};
      });
  }

  /**
   * Returns info all classes properties
   *
   * @param _class
   * @param lang
   */
  public findPropertiesByClassId(_class: string, lang?: string): Observable<Property[]> {
    return this.allProperties(lang)
      .map(properties => properties.filter(property => property.domain.indexOf(_class) > -1))
  }

  /**
   * Returns range that is of type select (alt)
   *
   * @param range
   * @param lang
   */
  public findRangeById(range: string, lang?: string): Observable<any> {
    return this.allRanges(lang)
      .map(ranges => ranges[range] || {})
  }
}
