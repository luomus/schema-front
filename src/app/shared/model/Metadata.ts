export interface Metadata {


  /**
   * Class name
   */
    class?: string;

  /**
   * showrt class name
   */
  shortName?: string;

  /**
   * label for the class
   */
  label?: string;

  /**
   * comment for the class
   */
  comment?: string;
}
