import { SchemaPage } from './app.po';

describe('schema App', function() {
  let page: SchemaPage;

  beforeEach(() => {
    page = new SchemaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
